package com.alan.alice;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainActivity extends Activity {
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.activity_main, menu);
	    return true;
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getWindow().requestFeature(Window.FEATURE_PROGRESS);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		WebView myWebView = (WebView) findViewById(R.id.webview);
		

		 myWebView.getSettings().setJavaScriptEnabled(true);

		 final Activity activity = this;
		  myWebView.setWebChromeClient(new WebChromeClient() {
		   public void onProgressChanged(WebView view, int progress) {
		     // Activities and WebViews measure progress with different scales.
		     // The progress meter will automatically disappear when we reach 100%
		     activity.setProgress(progress * 100);
		   }
		 });
		 myWebView.setWebViewClient(new WebViewClient() {
		   public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
		     Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
		   }
		 });

		myWebView.loadUrl("http://demo.vhost.pandorabots.com/pandora/talk?botid=e02728fd9e340364");
		
	}

	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.menu_settings:
	            emailerror();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	private void emailerror() {
		final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.setType("plain/text");
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"astropiloto@gmail.com"});
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "An error with ALICE");
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I wish to report an error.");
		startActivity(Intent.createChooser(emailIntent, "Send mail..."));
		
	}
	

}
